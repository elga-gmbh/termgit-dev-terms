<ClaML version="2.0.0">
    <Meta name="titleLong" value="RADS_LiverImaging"/>
    <Meta name="resource" value="ValueSet"/>
    <Meta name="id" value="rads-liverimaging"/>
    <Meta name="url" value="https://termgit.elga.gv.at/ValueSet/rads-liverimaging"/>
    <Meta name="preliminary" value="true"/>
    <Meta name="status" value="draft"/>
    <Meta name="description_eng" value="Value set for the diagnostic imaging report (v3). The value set will remain in draft status until the codes will be replaced by SNOMED CT codes (presumably with Austrian extension February 2025 release). Users are advised not to use the value set before it enters the active stage. Note: The Reporting and Data Systems (RADS) assessment scales are a registered trademark of the American College of Radiology."/>
    <Meta name="description" value='Value-Set für den Befund bildgebende Diagnostik (V3). Das Value-Set verbleibt im Entwurf-Status "draft", bis die Codes durch SNOMED-CT-Codes ersetzt werden (voraussichtlich mit dem Austrian-Extension-Release im Februar 2025). Den Benutzer:innen wird empfohlen, das Value-Set nicht zu nutzen, solange es noch nicht aktiviert ist (Status "active"). Achtung: Die "Reporting and Data Systems (RADS)"-Skalen sind eine eingetragene Marke des American College of Radiology.'/>
    <Meta name="website" value="https://www.acr.org/"/>
    <Meta name="count" value="8"/>
    <Identifier uid="1.2.40.0.34.6.0.10.92"/>
    <Title name="rads-liverimaging" version="0.1.1+20240610" date="2024-06-10">RADS_LiverImaging</Title>
    <Authors>
        <Author name="American College of Radiology">url^https://www.acr.org/^^^^</Author>
    </Authors>
    <ClassKinds>
        <ClassKind name="code"/>
    </ClassKinds>
    <RubricKinds>
        <RubricKind name="preferred"/>
    </RubricKinds>
    <Class code="LR-NC">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-NC</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category NC) - Not categorizable (due to image omission or degradation)</Label>
        </Rubric>
    </Class>
    <Class code="LR-1">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-1</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category 1) - Definitely benign</Label>
        </Rubric>
    </Class>
    <Class code="LR-2">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-2</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category 2) - Probably benign</Label>
        </Rubric>
    </Class>
    <Class code="LR-3">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-3</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category 3) - Intermediate probability of malignancy</Label>
        </Rubric>
    </Class>
    <Class code="LR-M">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="false"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="S"/>
        <Rubric kind="preferred">
            <Label>LR-M</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category 4) - Probably or definitely malignant, not necessarily HCC</Label>
        </Rubric>
    </Class>
    <Class code="LR-4">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="1"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-4</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category 4) - Probably HCC</Label>
        </Rubric>
    </Class>
    <Class code="LR-5">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="1"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-5</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category 5) - Definitely HCC</Label>
        </Rubric>
    </Class>
    <Class code="LR-TIV">
        <Meta name="codeSystem" value="http://terminology.hl7.org/CodeSystem/ACR"/>
        <Meta name="codeSystemVersion" value="2.0.1"/>
        <Meta name="TS_ATTRIBUTE_ISLEAF" value="true"/>
        <Meta name="Level" value="0"/>
        <Meta name="Type" value="L"/>
        <Rubric kind="preferred">
            <Label>LR-TIV</Label>
        </Rubric>
        <Rubric kind="designation">
            <Label>en-US|^^^^|Liver assessment (category TIV) - Tumor in vein</Label>
        </Rubric>
    </Class>
</ClaML>
