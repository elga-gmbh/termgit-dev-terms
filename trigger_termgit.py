from subprocess import PIPE, STDOUT, Popen
import sys
from urllib import parse, request, error
import json
import os
import argparse

# Name of the trigger token in the Termgit Project (CI/CD Settings) that is used to trigger the Termgit pipeline
TRIGGER_TOKEN_NAME = "AUTOMATIC_TOKEN_TERMINOLOGIES"

def _env(name: str) -> str:
    """ 
    Takes the name of an environment variable as input and returns its value. If the variable does not exist, an empty string is returned. 
    """
    if name not in os.environ:
        print(f"Warning: Environment variable ${{{name}}} does not exist")
    return os.environ.get(name, "")

def url_encode(project: str) -> str:
    """
    Takes a project name or path as input and encodes it for use in URLs. It replaces special characters with their corresponding percent-encoded values. For example, the project name "my/project" will be encoded as "my%2Fproject".
    """
    return parse.quote(project, safe="")


def default_header() -> dict[str, str]:
    """ 
    Returns the default header dictionary used in the HTTP requests containing the private token and the content type.
    """
    return {
        "PRIVATE-TOKEN": _env("GITLAB_CI_TOKEN"),
        "Content-Type": "application/json",
    }


def get_trigger_token() -> str:
    """
    Retrieves the trigger token associated with a specific project and trigger name through the Gitlab API. If no trigger token is found, an empty string is returned. 
    The trigger token is used for authentication when triggering actions or events in the project. 
    """
    print(f"Retrieving trigger token for project {_env('TERMGIT_PROJECT')} with the name '{TRIGGER_TOKEN_NAME}'")

    # Get all triggers for the project
    req = request.Request(url=_env("PROJECT_URL") + "/triggers", headers=default_header(),  data=None, method="GET")
    resp = request.urlopen(req)

    # Convert the response to a JSON object
    resp_json = json.loads(resp.read().decode("utf-8"))
    
    # Find the trigger token with the correct name
    for trigger in resp_json:
        if trigger["description"] == TRIGGER_TOKEN_NAME and len(trigger["token"]) > 4:
            return trigger["token"]
    
    print("No trigger token found")
    return ""

def create_trigger_token() -> str:
    """
    Creates a new trigger token in the specified project with the given name. Returns the generated token. 
    """
    print(f"Creating new trigger token for project {_env('TERMGIT_PROJECT')} with the name '{TRIGGER_TOKEN_NAME}'")
    
    data: dict = {
        "description": TRIGGER_TOKEN_NAME
    }
    data_bytes = json.dumps(data).encode("utf-8")

    # Send the trigger token creation request
    req = request.Request(url=_env("PROJECT_URL") + "/triggers", headers=default_header(), data=data_bytes, method="POST")
    resp = request.urlopen(req)
    resp_json = json.loads(resp.read().decode("utf-8"))

    return resp_json["token"]

def cancel_previous_pipelines():
    """
    Cancels previous pipelines for the specified project. Only cancels pipelines that meet the following criteria:
    - Pipeline is on the correct branch (TERMGIT_PIPELINE_BRANCH)
    - Pipeline is triggered via a trigger or an API call (for now: also for push pipelines)
    - Pipeline is in status "manual", "pending", "created" or "running"
    """
    print(f"Canceling previous pipelines for project {_env('TERMGIT_PROJECT')}")

    # Get all pipelines for the project on the specified branch
    req_url = _env("PROJECT_URL") + "/pipelines" + "?ref=" + _env("TERMGIT_PIPELINE_BRANCH")
    req = request.Request(url=req_url, headers=default_header(), method="GET")
    resp = request.urlopen(req)
    resp_json = json.loads(resp.read().decode("utf-8"))
    
    # Abort if there are no pipelines
    if len(resp_json) == 0:
        print("No old pipelines found.")
        return
    
    # Cancel the first pipelines that meet the criteria
    for pipeline in resp_json[:10]:
        correct_ref = pipeline["ref"] == _env("TERMGIT_PIPELINE_BRANCH")
        correct_source = pipeline["source"] in ["trigger", "api", "push"]
        correct_status = pipeline["status"] in ["manual", "pending", "created", "running"]

        # Cancel the pipeline if it meets the criteria: correct branch, correct source, correct status
        if correct_ref and correct_source and correct_status:
            print(f"Cancelling: {pipeline['id']=}, {pipeline['status']=}, {pipeline['source']=}, {pipeline['ref']=}, {pipeline['sha']=}") 

            # Send the pipeline cancellation request
            req = request.Request(url=_env("PROJECT_URL") + "/pipelines/" + str(pipeline["id"]) + "/cancel", headers=default_header(),  data=None, method="POST")
            resp = request.urlopen(req)
            resp_json = json.loads(resp.read().decode("utf-8"))
        else:
            print(f"Skipping: {pipeline['id']=}, {pipeline['status']=}, {pipeline['source']=}, {pipeline['ref']=}, {pipeline['sha']=}") 


def trigger_pipeline(trigger_token, branch_name): 
    """
    Triggers the pipeline for the specified project on the given branch through the Gitlab API. If the corresponding branch does not exist yet, it will be created from the Termgit default branch (TERMGIT_PROJECT_BRANCH). 
    Returns the response JSON from the pipeline trigger request.
    """
    print(f"Triggering pipeline for project {_env('TERMGIT_PROJECT')} on branch {branch_name}")
    

    # === 1. Create new branch if corresponding branch does not exist yet (https://docs.gitlab.com/ee/api/branches.html#create-repository-branch)

    # Send the branch creation request. 
    # 'branch' is the name of the new branch, 'ref' is the name of the branch from which the new branch is created
    req_url = _env("PROJECT_URL") + f"/repository/branches?branch={branch_name}&ref=" + _env("TERMGIT_PROJECT_BRANCH")
    print(f"Sending branch creation request to: {req_url}")
    req = request.Request(url=req_url, headers=default_header(), method="POST")

    # Try to create the branch. If the branch already exists, the request will fail with HTTP Error 400
    try:
        resp = request.urlopen(req)
        resp_json = json.loads(resp.read().decode("utf-8"))
        if 'name' in resp_json:
            print(f"New branch {branch_name} created from {_env('TERMGIT_PROJECT_BRANCH')}")
    except error.HTTPError as e:
        resp_json = {}
        if e.code == 400:
            print(f"Branch {branch_name} already exists.")
        else:
            print(f"HTTP Error: {e.code} {e.reason}")
            sys.exit(1)

    # === 2. Trigger the pipeline (https://docs.gitlab.com/ee/api/pipelines.html#create-a-new-pipeline)

    # Prepare the data for the pipeline trigger request
    data: dict = {
        "ref": branch_name,
        "token": trigger_token,
        "variables": {
            "TRIGGER_PIPELINE_ID": _env("CI_PIPELINE_ID"),
            "TRIGGER_PIPELINE_URL": _env("CI_PIPELINE_URL"),
            "TRIGGER_PROJECT_NAME": _env("CI_PROJECT_TITLE"),
            "TRIGGER_PROJECT_ID": _env("CI_PROJECT_ID"),
            "TRIGGER_PROJECT_PATH": _env("CI_PROJECT_PATH"),
            "TRIGGER_COMMIT_SHA": _env("CI_COMMIT_SHORT_SHA"),
            "TRIGGER_BRANCH": _env("CI_COMMIT_BRANCH"),
            "TRIGGER_COMMIT_TITLE": _env("CI_COMMIT_TITLE"),
            "TRIGGER_JOB_WAITING": "waiting_for_termgit"
        }
    }

    print("Additional environment variables that are sent to the Termgit pipeline: ")
    print(data['ref'])
    print(data['variables'])

    # Convert the data to a byte string
    data_bytes = json.dumps(data).encode("utf-8")

    # Send the pipeline trigger request
    req_url = _env("PROJECT_URL") + "/trigger/pipeline"
    print(f"Sending pipeline trigger request to: {req_url}")

    req = request.Request(url=req_url, headers=default_header(), data=data_bytes, method="POST")
    resp = request.urlopen(req)
    resp_json = json.loads(resp.read().decode("utf-8"))

    return resp_json

if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="Trigger the pipeline in the Termgit Project")
    
    parser.add_argument("--testing", action="store_true", help="Use default values for testing, otherwise use env variables")
    parser.add_argument("--private-token", type=str, help="Private token for Gitlab API if testing is enabled")

    parser.add_argument("--cancel", action="store_true", help="Cancel previous pipelines")
    parser.add_argument("--trigger", action="store_true", help="Trigger the pipeline")
    args = parser.parse_args()
    
    # Check if all environment variables are set and exit if not
    env_missing = False
    for var in ["GITLAB_CI_TOKEN", "GITLAB_HOST", "TERMGIT_PROJECT", "TERMGIT_PROJECT_BRANCH", "TERMGIT_PIPELINE_BRANCH"]:
        if not _env(var):
            env_missing = True
    if env_missing:
        print("Error: Not all environment variables are set. Exiting.")
        sys.exit(1)

    # Set the project URL environment variable
    os.environ["PROJECT_URL"] = "https://" + _env("GITLAB_HOST") + "/api/v4/projects/" + url_encode(_env("TERMGIT_PROJECT"))
    
    # Try to trigger the pipeline
    try:
        # get the trigger token
        trigger_token = get_trigger_token()
        
        # create a new trigger token if none was found
        if not trigger_token:
            trigger_token = create_trigger_token()

        # cancel previous pipelines if requested
        if args.cancel:
            cancel_previous_pipelines()
        
        # trigger the termgit pipeline if requested
        if args.trigger:
            new_pipeline = trigger_pipeline(trigger_token, _env("TERMGIT_PIPELINE_BRANCH"))
            print(f"Triggered Pipeline: {new_pipeline['web_url']}")

            with open("termgit_settings.env", "a") as env_file:
                env_file.write(f"TERMGIT_PROJECT_ID={new_pipeline['project_id']}\n")
                env_file.write(f"TERMGIT_PIPELINE_ID={new_pipeline['id']}\n")

    # Exit on HTTP errors
    except error.HTTPError as e:
        print(f"HTTP Error: {e.code} - {e.reason}")
        sys.exit(1)
    except error.URLError as e:
        print(f"URL Error: {e.reason}")
        sys.exit(1)
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        sys.exit(1)
    sys.exit(0)