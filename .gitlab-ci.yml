default:
  # all jobs are interruptible, thus they will be cancelled if a newer pipeline starts before a job is completed
  interruptible: true

variables:
  SKIP_MALAC:
    description: "If set to true, the Termgit pipeline will skip the malac conversion."
    value: "false"
    options: 
      - "true"
      - "false"
  
  AUTOMATIC_COMMIT_PREFIX: "AUTOMATIC_COMMIT"
  AUTOMATIC_COMMIT_UPDATE_TERMINOLOGIES: "${AUTOMATIC_COMMIT_PREFIX} update terminologies"

  GIT_DEPTH: 1
  GIT_STRATEGY: clone

stages:
  - manual_trigger 
  - prepare_env
  - trigger_termgit 
  - waiting_for_termgit 

# A YAML anchor (&are_vars_set) that defines a bash function to check if all required environment variables are set
.are_vars_set: &are_vars_set
  - |
    are_vars_set () { 
      message_template=$1
      shift
      exitcode=0
      for var in "$@"; do
        if [[ -z "${!var}" ]]; then 
          echo $(printf "$message_template" "$var"); exitcode=1;
        fi
      done
      return $exitcode
    }

manual_trigger:
  tags:
    - $RUNNER_TAG
  stage: manual_trigger
  variables:
    GIT_STRATEGY: none
  rules:
    # Job will run in manual mode if the following conditions are met:
    # - It is a commit on a branch (not for a tag)
    # - It is not the default branch
    # - The commit title does not start with "AUTOMATIC_COMMIT"
    # - The pipeline was not triggered by a schedule
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_COMMIT_TITLE !~ /^AUTOMATIC_COMMIT.*/ && $CI_PIPELINE_SOURCE != "schedule"
      when: manual
  script:
    - echo $CI_COMMIT_BRANCH

prepare_env:
  image: redhat/ubi8-minimal:8.8
  tags:
    - $RUNNER_TAG
  stage: prepare_env
  variables: 
    GIT_STRATEGY: none
  rules:
    # Job will run if it is a commit on a branch (not for a tag) and the commit title does not start with "AUTOMATIC_COMMIT"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_TITLE !~ /^AUTOMATIC_COMMIT.*/
    # Job will run if it was triggered by a schedule
    - if: $CI_PIPELINE_SOURCE == "schedule" 

  before_script:
    - *are_vars_set
    - |
      dep_vars=( 
        TERMGIT_PROJECT 
        TERMGIT_PROJECT_BRANCH 
        GITLAB_CI_TOKEN 
        GITLAB_HOST 
      )
      # Check if all required environment variables are set
      are_vars_set "Environment variable %s is not set. Create a CI/CD variable with that name." "${dep_vars[@]}" || exit 1

  script:
    # Check if the CI_COMMIT_TITLE contains the SKIP_MALAC flag and set the corresponding environment variable
    - | 
      if [[ "${CI_COMMIT_TITLE}" == *"SKIP_MALAC"* ]]; then
        echo "INFO: SKIP_MALAC is set to true"
        export SKIP_MALAC=true
        echo "SKIP_MALAC=true" >> flags.env
      fi

    - echo "CI_COMMIT_TITLE=${CI_COMMIT_TITLE}"
    - echo "SKIP_MALAC=${SKIP_MALAC}"
  artifacts:
    reports:
      dotenv: flags.env
    expire_in: 1 week


trigger_termgit:
  tags:
    - $RUNNER_TAG
  stage: trigger_termgit
  image: python:3.11
  rules:
    # Job will run if it is a commit on a branch (not for a tag)
    # and if commit title does NOT start with "AUTOMATIC_COMMIT"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_TITLE !~ /^AUTOMATIC_COMMIT.*/

    # Or if it was triggered by a schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"

  script:
    - echo "Triggering termgit pipeline" 

    # Output information if triggered by schedule
    - |
      if [[ "${CI_PIPELINE_SOURCE}" == "schedule" ]]; then
        echo "Pipeline triggered by schedule '${SCHEDULE_NAME}' at $(date)"
      fi
    
    # Check which branch on the Termgit-Project to trigger a pipeline on
    # If this is the default branch, trigger the pipeline on the branch that was set in TERMGIT_PROJECT_BRANCH
    # (which should be the default branch of the Termgit-Project)
    # If this is not the default branch, trigger the pipeline on a branch with the same name as this branch
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        export TERMGIT_PIPELINE_BRANCH="${TERMGIT_PROJECT_BRANCH}";
      else
        export TERMGIT_PIPELINE_BRANCH="${CI_COMMIT_BRANCH}";
      fi
    
    # Save the branch name in .env file as an artifact
    - echo "TERMGIT_PIPELINE_BRANCH=${TERMGIT_PIPELINE_BRANCH}" > termgit_settings.env

    # If SKIP_MALAC is set to true, add it to the ci_commit_title of the trigger request
    - |
      export CI_COMMIT_TITLE_NEW=""
      if [[ "${SKIP_MALAC}" == "true" ]]; then
        export CI_COMMIT_TITLE_NEW="${CI_COMMIT_TITLE_NEW}${SKIP_MALAC}"
      fi
      echo "CI_COMMIT_TITLE_NEW=${CI_COMMIT_TITLE_NEW}"
      echo "CI_COMMIT_TITLE=${CI_COMMIT_TITLE}"
      if [[ -n "${CI_COMMIT_TITLE_NEW}" ]]; then
        export CI_COMMIT_TITLE="${CI_COMMIT_TITLE_NEW}"
      fi

    # Execute python script to trigger a pipeline on the Termgit-Project
    - chmod u+x ./trigger_termgit.py
    - python trigger_termgit.py --trigger --cancel 2>&1

    # Save the response of the python script in a variable
    - termgit_pipeline_response=$?

    # Check if the curl request was successful, if not, exit with an error and stop the pipeline
    - |
      if [[ "${termgit_pipeline_response}" == 1 ]]; then
        echo "ERROR: Could not trigger a pipeline on the Termgit-Project."
        exit 1
      elif [[ "${termgit_pipeline_response}" == 2 ]]; then
        echo "Not triggering a pipeline, as a different one from this branch is still running."
        exit 1
      else
        cat termgit_settings.env
        echo "${CI_JOB_NAME} has finished"
        exit 0
      fi

waiting_for_termgit:
  tags:
    - $RUNNER_TAG
  stage: waiting_for_termgit
  image: python:3.11
  rules:
    # This job will wait for Termgit if the following conditions are met:
    # - It is a commit on a branch (not for a tag)
    # - The commit title does not start with "AUTOMATIC_COMMIT"
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_TITLE !~ /^AUTOMATIC_COMMIT.*/
      when: manual
    # Or if it was triggered by a schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: manual

  script:
    # Check if Termgit sent a cancel request
    # If yes, exit with error and stop the pipeline
    - |
      if [[ "${CANCEL_JOB}" == "true" ]]; then 
        echo "=========================================="
        echo "CANCEL_JOB=${CANCEL_JOB}"
        echo "WARNING: Termgit sent a cancel request. A previous job in Termgit likely failed. "
        echo "=========================================="
        exit 1 
      elif [[ "${CANCEL_JOB}" == "false" ]]; then
        echo "=========================================="
        echo "CANCEL_JOB=${CANCEL_JOB}"
        echo "Execution started by Termgit, pipeline finished successfully." 
        echo "=========================================="
      else
        echo "=========================================="
        echo "ERROR: CANCEL_JOB is not set - did someone manually start this job?"
        echo "=========================================="
        exit 1
      fi

    - echo "${CI_JOB_NAME} has finished"

