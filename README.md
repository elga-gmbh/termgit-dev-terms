# Terminologies Project

## CI/CD Variables

### Terminologies Project

This project needs the following CI/CD variables to be set in the GitLab project settings:

**GITLAB_CI_TOKEN**: a personal access token with enough permissions to start pipelines on the termgit project.  
**GITLAB_HOST**: the GitLab host, e.g. "gitlab.com"  
**TERMGIT_PROJECT**: the path to the termgit project, e.g. "elga-gmbh/termgit-dev"  
**TERMGIT_PROJECT_BRANCH**: the branch of the termgit project, e.g. "dev"  


### Termgit Project 

Termgit also needs new CI/CD variables to be set in the GitLab project settings:

**GITLAB_CI_TOKEN**: a personal access token with enough permissions to start pipelines on the terminologies project.  
**GITLAB_HOST**: the GitLab host, e.g. "gitlab.com"  


